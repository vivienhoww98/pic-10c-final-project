# README for Final Project: Big 2 #

This is my final project for my Winter 2017 PIC 10C course. 

### The rules of the game ###

* There are 4 players. 13 cards are dealt to each player.
* The human player will be playing against 3 computer-controlled players.
* The goal of the game is to be the first player to get rid of all cards in hand.
* The player with 3ofD in their hand starts the game.
* Each player will discard a card during their turn. The player can also decide to pass if they have no valid moves or if they are saving up their cards for a certain strategy.
* A player can only discard cards that are larger than the last discarded card.
* Therefore, the player is encouraged to discard 3ofD for their first play.
* The cards are ranked from 3 to 2 with 3 as the smallest and 2 as the largest.
* 3 < 4 < 5 < 6 < 7 < 8 < 9 < T < J < Q < K < A < 2
* If two cards have the same rank, the suit will be the tie-breaker.
* The order of suits, from smallest to largest, is Diamonds < Clubs < Hearts < Spades
* The cards will be arranged in ascending order for convenience.
* The game ends when someone runs out of cards.

*****************************************************************************************

I have attempted to implement a simple version of a Big 2 card game. Big 2 is a card game of Chinese origin. It is quite popular in Malaysia and is often played during Chinese New Year or school trips. The game is played using a standard deck of 52 cards.

This game is a simplified version of the actual card game. The actual game allows players to discard up to 5 cards at a time. My version only allows the player to discard one card at a time for simplicity.

### What have I implemented so far? ###

So far, I have successfully implemented:

* a mechanism to help me shuffle the cards in a deck. I used a random combination of the generic algorithms std::reverse and std::next_permutation to help me achieve this. As std::next_permutation sorts the cards in ascending order before giving me the next permutation, I had to pass std::next_permutation a nonsense sorting order lambda function so that std::next_permutation will not destroy my efforts of randomizing the cards thoroughly.
* a mechanism to determine which player should go first (according to the rules, the player with 3ofD will go first)
* a pass mechanism to allow the correct player to resume play after a certain amount of passes (as a player can only discard cards that are larger than the last discarded card, if no one can beat the last discarded card, they pass, and the player who discarded that largest card is allowed to reset the discard pile with a small card.)
* cycling between players correctly
* different actions for when the player is human or the computer
* blocks of code to check for valid card input, valid picks, and valid moves

etc. among other mechanisms.

*****************************************************************************************

### How does this relate to class material? ###

* This game utilizes the string and vector classes which conform to the principles of RAII.
These classes manage heap memory by containing all calls to new and delete inside the class.
They also treat acquired memory as if they were initialized with it.
* The code features the use of iterators to iterate through vectors.
* Most of the game involves iterating through a vector of players between turns.
* The code features generic algorithms that accept iterators as parameters.
* Some of the generic algorithms I used include: std::next_permutation, std::reverse, std::find, and std::binary_search
* Before writing a particular function, I would look at <algorithm> to see if there are any generic algorithms that perform the same task.
* The code features lambda functions.
* As I needed a nonsense sorting order function to help me randomize my cards, it did not make sense for me to overload operator() for a class. An inline lambda function was the best option.
* I use version control (Git and Bitbucket) to help me keep track of my changes.