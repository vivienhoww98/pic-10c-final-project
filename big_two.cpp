#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include "card.h"
#include "deck.h"
#include "hand.h"
#include "player.h"
#include <iomanip>

int main(){

	// A game has four Players
	Player player1 = Player(); // Human
	Player player2 = Player(); // Computer
	Player player3 = Player(); // Computer
	Player player4 = Player(); // Computer

	// Vector of Players with its iterator
	std::vector<Player*> the_players = { &player1, &player2, &player3, &player4 };
	std::vector<Player*>::iterator current = the_players.begin();

	// Deck with 52 Cards
	Deck my_deck = Deck();
	// Shuffle and deal cards to Players
	my_deck.shuffle_deck();
	my_deck.deal_deck(player1, player2, player3, player4);

	// Uncomment for debugging
	// player1.print_cards(); player2.print_cards(); player3.print_cards(); player4.print_cards();

	// The game welcome message
	std::cout << "\nWELCOME TO BIG 2!\n";
	std::cout << "The rules of the game are in the README. \n";
	std::cout << "Have fun!\n";

	// Mechanism to determine which player should go first
	// The player with 3ofD will go first
	for (auto iter = the_players.begin(), stop = the_players.end(); iter != stop; ++iter){
		Card first_card = ((*iter)->get_cards()[0]);
		if (first_card == Card(DIAMONDS, THREE)){
			current = iter;
			break;
		}
	}

	std::string answer = "";
	std::string ifpassanswer = "";
	std::string card_picked = "";
	Card picked = Card(DIAMONDS, THREE);
	Card previous_move = Card(DIAMONDS, THREE);
	int pass_count = 0;

	bool playgame = true;
	while (playgame == true){

		if (*current == &player1){

			// Marks start of Human's turn
			std::cout << std::setfill('-') << std::setw(90) << std::endl;
			std::cout << "\nYour cards: " << std::endl;
			(*current)->print_cards();
			std::cout << "\nLast discarded card: " << std::endl;
			previous_move.print_card();
			std::cout << std::endl;

			bool ask = true;
			while (ask == true){

				std::cout << "\nDISCARD a card [D] or PASS [P]? \n";
				std::getline(std::cin, answer);

				if (answer == "D"){

					bool getcard = true;
					while (getcard == true){

						std::cout << "Select a card to discard: ";
						std::cout << "(Example: Enter [2ofS] for the Two of Spades) \n";
						std::getline(std::cin, card_picked);

						if (valid_card(card_picked) == false){
							std::cout << "Invalid card!\n";
							getcard = true;
							continue; // go back to start and ask again
						}

						// to reach here, valid_card == true
						picked.process_card(card_picked);

						if (valid_pick(current, picked) == false){
							std::cout << "You do not have this card in your hand! \n";
							getcard = true;
							continue; // go back to start and ask again
						}

						// to reach here, valid_pick == true

						if (valid_move(previous_move, picked) == false){

							std::cout << "Invalid card!\n";
							getcard = true;

							bool askifpass = true;
							while (askifpass == true){

								std::cout << "There might not be any valid moves. Do you want to PASS? Enter [Y] or [N] \n";
								std::getline(std::cin, ifpassanswer);

								if (ifpassanswer == "Y"){
									std::cout << "You decided to pass!\n";
									std::cout << "                                      Pass\n";
									++pass_count;
									askifpass = false;
									getcard = false;
								}
								else if (ifpassanswer == "N"){
									std::cout << "Okay... If you insist... \n";
									askifpass = false;
									getcard = true;
								}
								else{ // answer == anything else
									std::cout << "Invalid choice!\n";
									askifpass = true;
								}
							}
						}
						else{ //valid_move == true
							(*current)->discard(picked);
							previous_move = picked;
							pass_count = 0; //reset pass count as a card was picked
							std::cout << "You discarded: " << std::endl;
							std::cout << "                                      "; picked.print_card(); std::cout << std::endl;
							getcard = false;
						}
					}
					ask = false;
				}
				else if (answer == "P"){
					std::cout << "\nYou decided to pass!\n";
					std::cout << "                                      Pass\n";
					++pass_count;
					ask = false;
				}
				else{ // answer == anything else
					std::cout << "\nInvalid choice!\n";
					ask = true;
				}
			}
		}
		else{

			// Marks start of Computer's turn
			std::cout << std::setfill('-') << std::setw(98) << std::endl;

			// Cycles through every card in hand
			// For each cycle, it picks a card
			// If the move is valid, it discards the card
			// If the end of the hand is reached, and no move is valid, it passes
			for (auto iter = (*current)->get_cards().begin(), stop = (*current)->get_cards().end(); iter != stop; ++iter){
				picked = *iter;
				if (valid_move(previous_move, picked) == true){
					(*current)->discard(picked);
					previous_move = picked;
					pass_count = 0; //reset pass count as a card was picked
					std::cout << "\nComputer discarded: " << std::endl;
					std::cout << "                                      "; picked.print_card(); std::cout << std::endl;
					break;
				}
				if ((valid_move(previous_move, picked) == false) && (picked == *(stop - 1))){
					std::cout << "\nComputer decided to pass!" << std::endl;
					std::cout << "                                      Pass\n";
					++pass_count;
					break;
				}
			}
			
			std::cout << "\nComputer's cards: " << std::endl;
			(*current)->print_cards_back();

			// Uncomment for debugging
			//(*current)->print_cards();			
		}

		// Checks if anyone has discarded all their cards
		// If someone's hand is empty, the game ends
		for (auto piter = the_players.begin(), stop = the_players.end(); piter != stop; ++piter){
			bool if_empty = (*piter)->get_cards().empty();
			if (if_empty){
				std::cout << std::setfill('-') << std::setw(110) << std::endl;
				std::cout << "\n\nSomeone has won! Gameplay ends.\n";
				playgame = false;
				break;
			}
			else
				playgame = true;
		}

		// Resets discard pile if there has been 3 consecutive passes
		// In short, a certain player gets two turns in a row
		if (pass_count == 3){
			// reset previous_move
			previous_move = Card(DIAMONDS, THREE);
			std::cout << "\nThe discard pile has been reset!\n";
			// reset pass_count
			pass_count = 0;
		}

		// Cycles to next player
		if (current == (the_players.end() - 1))
			current = the_players.begin();
		else
			++current;
	}

	// Displays winner, message depends on whether player is a human or the computer
	for (auto piter = the_players.begin(), stop = the_players.end(); piter != stop; ++piter){
		bool if_empty = (*piter)->get_cards().empty();
		if (if_empty){
			if (*piter == &player1){
				std::cout << "\nYou win! Congratulations!\n";
				break;
			}
			else
				std::cout << "\nThe computer wins! Too bad for you!\n";
		}
	}

	std::cout << "\nCards remaining: \n";
	std::cout << "Player 1: \n"; player1.print_cards();
	std::cout << "Player 2: \n"; player2.print_cards();
	std::cout << "Player 3: \n"; player3.print_cards();
	std::cout << "Player 4: \n"; player4.print_cards();

	std::cout << "\nGAME OVER! BYE!\n" << std::endl;

	return 0;
}