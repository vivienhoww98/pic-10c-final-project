#include "card.h"
#include "deck.h"
#include "hand.h"
#include "player.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

// Card Class constructor
// Initializes a Card with the lowest suit and rank
Card::Card(suit_t pick_suit, rank_t pick_rank){
	suit = pick_suit;
	rank = pick_rank;
}

// Returns the rank of a Card in the form of a string
// Will be useful for printing
std::string Card::return_rank() const{
	std::string rank_name;
	switch (rank) {
	case THREE: rank_name = "3"; break;
	case FOUR: rank_name = "4"; break;
	case FIVE: rank_name = "5"; break;
	case SIX: rank_name = "6"; break;
	case SEVEN: rank_name = "7"; break;
	case EIGHT: rank_name = "8"; break;
	case NINE: rank_name = "9"; break;
	case TEN: rank_name = "T"; break;
	case JACK: rank_name = "J"; break;
	case QUEEN: rank_name = "Q"; break;
	case KING: rank_name = "K"; break;
	case ACE: rank_name = "A"; break;
	case TWO: rank_name = "2"; break;
	default: break;
	}
	return rank_name;
}

// Returns the suit of a Card in the form of a string
// Will be useful for printing
std::string Card::return_suit() const{
	std::string suit_name;
	switch (suit) {
	case DIAMONDS: suit_name = "D"; break;
	case CLUBS: suit_name = "C"; break;
	case HEARTS: suit_name = "H"; break;
	case SPADES: suit_name = "S"; break;
	default: break;
	}
	return suit_name;
}

// Returns the rank of a Card in the form of an int
// Will be useful for comparisons
// As 3 is the lowest rank, it adds 3 to the enum
int Card::get_rank() const{
	return static_cast<int>(rank)+3;
}

// Returns the suit of a Card in the form of an int
// Will be useful for comparisons
// Adds 1 to the enum so that no suit returns 0
int Card::get_suit() const{
	return static_cast<int>(suit)+1;
}

// Compares two Cards
// Returns true if *this.rank < card2.rank
// If ranks are equal, returns true if *this.suit < card2.suit
bool Card::operator<(const Card& card2) const{
	if (rank != card2.rank)
		return rank < card2.rank;
	else
		return suit < card2.suit;
}

// Checks if two Cards are equal
// Returns true if cards are equal in rank and suit and false otherwise
bool Card::operator==(const Card& card2) const{
	if (rank != card2.rank)
		return false;
	else if (suit != card2.suit)
		return false;
	return true;
}

// Prints the rank and suit of a Card in a certain format
void Card::print_card() const{
	std::cout << return_rank() << "of" << return_suit() << "  ";
}

// Converts a Card of user input to a Card that the program can work with
Card& Card::process_card(std::string& cp){
	switch (cp[0]) {
	case '3': rank = THREE; break;
	case '4': rank = FOUR; break;
	case '5': rank = FIVE; break;
	case '6': rank = SIX; break;
	case '7': rank = SEVEN; break;
	case '8': rank = EIGHT; break;
	case '9': rank = NINE; break;
	case 'T': rank = TEN; break;
	case 'J': rank = JACK; break;
	case 'Q': rank = QUEEN; break;
	case 'K': rank = KING; break;
	case 'A': rank = ACE; break;
	case '2': rank = TWO; break;
	default: break;
	}
	switch (cp[3]) {
	case 'D': suit = DIAMONDS; break;
	case 'C': suit = CLUBS; break;
	case 'H': suit = HEARTS; break;
	case 'S': suit = SPADES; break;
	default: break;
	}
	return *this;
}