#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

#ifndef CARD_H
#define CARD_H

class Card;
class Deck;
class Hand;
class Player;

enum suit_t { DIAMONDS, CLUBS, HEARTS, SPADES };
enum rank_t { THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE, TWO };

class Card{

public:
	Card(suit_t pick_suit, rank_t pick_rank);
	std::string return_rank() const;
	std::string return_suit() const;
	int get_rank() const;
	int get_suit() const;
	bool operator<(const Card& card2) const;
	bool operator==(const Card& card2) const;
	void print_card() const;

	Card& process_card(std::string& cp);

private:
	suit_t suit;
	rank_t rank;

};

#endif