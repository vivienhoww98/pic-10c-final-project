#include "card.h"
#include "deck.h"
#include "hand.h"
#include "player.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

// Deck Class constructor
// Initializes the Deck with 52 Cards in brand-new order
Deck::Deck(){

	suit_t s;
	rank_t r;

	for (int i = 0; i < 4; ++i){

		switch (i + 1) {
		case 1: s = DIAMONDS; break;
		case 2: s = CLUBS; break;
		case 3: s = HEARTS; break;
		case 4: s = SPADES; break;
		default: break;
		}

		for (int j = 0; j < 13; ++j){

			switch (j + 1) {
			case 1: r = THREE; break;
			case 2: r = FOUR; break;
			case 3: r = FIVE; break;
			case 4: r = SIX; break;
			case 5: r = SEVEN; break;
			case 6: r = EIGHT; break;
			case 7: r = NINE; break;
			case 8: r = TEN; break;
			case 9: r = JACK; break;
			case 10: r = QUEEN; break;
			case 11: r = KING; break;
			case 12: r = ACE; break;
			case 13: r = TWO; break;
			default: break;
			}

			main_deck.push_back(Card(s, r));

		}
	}
}

// Prints the Deck in a certain format
// Used for debugging purposes
void Deck::print_deck() const {

	for (int i = 0, n = main_deck.size(); i < n; ++i){
		main_deck[i].print_card();
		if ((i + 1) % 13 == 0)
			std::cout << std::endl;
	}
}

// Scrambles the ordered Deck of Cards. Stimulates real-life shuffling.
void Deck::shuffle_deck(){

	std::srand(time(nullptr));

	//lambda function for nonsense sorting order
	auto AllEqual = [](const int& a, const int& b){return (b*b) < a; };

	// Shuffles a vector of ints first
	std::vector<int> v(52);
	for (size_t i = 0, n = v.size(); i < n; ++i)
		v[i] = i;
	for (size_t i = 0; i < rand(); ++i)
		std::next_permutation(v.begin(), v.end(), AllEqual);
	std::reverse(v.begin(), v.end());
	for (size_t i = 0; i < rand(); ++i)
		std::next_permutation(v.begin(), v.end(), AllEqual);
	std::reverse(v.begin(), v.end());

	// Uses the shuffled vector of ints as indices to "shuffle" the main_deck
	std::vector<Card> copy;
	for (size_t i = 0, n = v.size(); i < n; ++i)
		copy.push_back(main_deck[v[i]]);

	main_deck = copy;
}

// Deals a shuffled Deck of Cards to 4 Players and sorts them
void Deck::deal_deck(Player& p1, Player& p2, Player& p3, Player& p4){

	for (size_t i = 0; i < 52; ++i){
		p1.get_cards().push_back(main_deck[i]);
		++i;
		p2.get_cards().push_back(main_deck[i]);
		++i;
		p3.get_cards().push_back(main_deck[i]);
		++i;
		p4.get_cards().push_back(main_deck[i]);
	}

	p1.sort_cards();
	p2.sort_cards();
	p3.sort_cards();
	p4.sort_cards();

}