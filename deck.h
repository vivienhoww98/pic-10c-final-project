#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

#ifndef DECK_H
#define DECK_H

class Card;
class Deck;
class Hand;
class Player;

class Deck{

public:
	Deck();
	void print_deck() const;
	void shuffle_deck();
	void deal_deck(Player& p1, Player& p2, Player& p3, Player& p4);

private:
	std::vector<Card> main_deck;

};

#endif