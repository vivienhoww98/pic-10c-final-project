#include "card.h"
#include "deck.h"
#include "hand.h"
#include "player.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

// Default Hand Constructor
// It is empty as it will be "constructed" via Deck::deal_cards()
Hand::Hand(){}

// Returns a vector of Cards
std::vector<Card>& Hand::get_hand(){
	return hand;
}

// Sorts the vector of Cards
void Hand::sort_hand(){
	std::sort(hand.begin(), hand.end());
}

// Prints the vector of Cards in a certain format
void Hand::print_hand() const{
	for (int i = 0, n = hand.size(); i < n; ++i)
		hand[i].print_card();
	std::cout << std::endl;
}

// Prints "Card" for each Card in the vector of Cards
// Used to print the computer's cards as their cards should be concealed from the human
void Hand::print_hand_back() const{
	for (int i = 0, n = hand.size(); i < n; ++i)
		std::cout << "Card  ";
	std::cout << std::endl;
}

// Discards the selected Card by deleting it from the vector of Cards
void Hand::discard_card(Card d){
	std::vector<Card>::iterator iter = std::find(hand.begin(), hand.end(), d);
	hand.erase(iter);
}