#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

#ifndef HAND_H
#define HAND_H

class Card;
class Deck;
class Hand;
class Player;

class Hand{

public:
	Hand::Hand();
	std::vector<Card>& get_hand();
	void sort_hand();
	void print_hand() const;
	void print_hand_back() const;

	void discard_card(Card d);
	
private:
	std::vector<Card> hand;

};

#endif