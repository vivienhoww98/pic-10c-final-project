#include "card.h"
#include "deck.h"
#include "hand.h"
#include "player.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

// Default Player Constructor
// It is empty as it will be "constructed" via Deck::deal_cards()
Player::Player(){};

// Returns the Cards in Hand
std::vector<Card>& Player::get_cards(){
	return cards.get_hand();
}

// Sorts the Cards in Hand
void Player::sort_cards(){
	cards.sort_hand();
}

// Prints the Cards in Hand in a certain format
void Player::print_cards() const{
	cards.print_hand();
}

// Prints "Card" for each Card in Hand
// Used to print the computer's cards as their cards should be concealed from the human
void Player::print_cards_back() const{
	cards.print_hand_back();
}

// Discards the selected Card by removing it from the Cards in Hand
void Player::discard(Card d){
	cards.discard_card(d);
}

// Checks if the user input a valid Card that satifies the conditions below
bool valid_card(std::string& card_picked){

	if (card_picked.length() != 4)
		return false;
	else if (card_picked[1] != 'o')
		return false;
	else if (card_picked[2] != 'f')
		return false;
	else if ((card_picked[3] != 'D') && (card_picked[3] != 'C') && (card_picked[3] != 'H') && (card_picked[3] != 'S'))
		return false;
	else if ((card_picked[0] != '3') && (card_picked[0] != '4') && (card_picked[0] != '5') && (card_picked[0] != '6') && (card_picked[0] != '7') &&
		(card_picked[0] != '8') && (card_picked[0] != '9') && (card_picked[0] != 'T') && (card_picked[0] != 'J') && (card_picked[0] != 'Q') &&
		(card_picked[0] != 'K') && (card_picked[0] != 'A') && (card_picked[0] != '2'))
		return false;
	else
		return true;
}

// Checks if the user actually owns the Card that they picked
bool valid_pick(std::vector<Player*>::iterator current, Card& picked){
	return std::binary_search((*current)->get_cards().begin(), (*current)->get_cards().end(), picked);
}

// Checks if the picked Card is larger than the previous Card
bool valid_move(Card& previous_card, Card& picked){
	if (previous_card < picked || previous_card == picked) //for first card
		return true;
	else
		return false;
}