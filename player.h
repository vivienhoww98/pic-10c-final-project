#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>

#ifndef PLAYER_H
#define PLAYER_H

class Card;
class Deck;
class Hand;
class Player;

class Player{

	// The Player class calls the functions of the Hand Class
	// This layering seems useless for the time being.
	// The reason for this layering is so that there is flexibility for
	// a money component to be added to the Player class later on

public:
	Player::Player();
	std::vector<Card>& get_cards();
	void sort_cards();
	void print_cards() const;
	void print_cards_back() const;

	void discard(Card d);

private:
	Hand cards;

};

// The "validifying" functions
// These functions will check for different aspects of validity before any Cards are discarded
bool valid_card(std::string& card_picked);
bool valid_pick(std::vector<Player*>::iterator current, Card& picked);
bool valid_move(Card& previous_card, Card& picked);

#endif